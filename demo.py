from concurrent.futures import thread
import io
import logging
import sys
from types import CoroutineType
from fastapi import FastAPI,HTTPException,File,UploadFile,Form
from fastapi.responses import HTMLResponse  
from infer.mage import predict
import uvicorn
from PIL import Image
import base64
import numpy as np
from typing import Optional
import cv2
from pydantic import BaseModel

app = FastAPI()

class Input(BaseModel):
    base64str : str
    threadhold: float

def base64str_to_PILImage(base64str):
    base64_img_bytes = base64str.encode('utf-8')
    decoded_data = base64.b64decode(base64_img_bytes)
    np_data = np.fromstring(decoded_data,np.uint8)
    img = cv2.imdecode(np_data,cv2.IMREAD_UNCHANGED)
    return img

@app.put("/predict")
def get_predictionbase64(d:Input):

    # Load the image
    img = base64str_to_PILImage(d.base64str)
    try:
        
        predicted_class = predict(img)
        clocks=[]
        for i in range(len(predicted_class)): 
            clock=str("Number of plate"+f' {i}'+':'+f' {predicted_class[i]} ')
            clocks.append(clock)
        return clocks
    except Exception as error:
        logging.exception(error)
        e = sys.exc_info()[1]
        raise HTTPException(status_code=500, detail=str(e))
